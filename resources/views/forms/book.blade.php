<div class="custom-field">
        <label>Name</label>
        <input placeholder="The Raven" class="form-control" name="name" type="text" value="{{ isset($book->name) ? $book->name : old('name') }}"/>
  </div>

  <div class="custom-field">
        <label>Author</label>
        <input placeholder="Edgar Allan Poe" class="form-control" name="author" type="text" value="{{ isset($book->author) ? $book->author : old('author') }}"/>
  </div>

  <div class="custom-field">
        <label>Published Date</label>
        <input placeholder="YYYY-MM" class="form-control datepicker" name="published_date" type="date" data-date-min-view-mode="months" data-date-format="yyyy-mm" value="{{ isset($book->published_date) ? $book->published_date : old('published_date') }}"/>
  </div>

  <div class="custom-field">
        <label>Category</label>
        <select name="category" class="form-control">
              @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
        </select>
  </div>

  <button class="btn btn-primary pull-right" type="submit">{{$buttonAction}}</button>
