@extends('layouts.app')
@section('content')
<h4>New Book</h3>
<form method="POST" action="{{ route('books.update', $book->id) }}" accept-charset="UTF-8">
    @include('forms.book', ['buttonAction' => 'Update'])
</form>
@stop
