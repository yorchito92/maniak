@extends('layouts.app')
@section('content')
      {{-- @include('includes.modal') --}}
      <form method="GET" action="{{ route('books.search') }}" accept-charset="UTF-8">
            <div id="searchBar">
                  <label>Filter</label>
                  <input name="search"/>
                  <label>By</label>
                  <select name="type" class="form-control">
                        <option value="name">Name</option>
                        <option value="author">Author</option>
                  </select>
                  <button class="btn btn-primary pull-right" type="submit">Filter</button>
            </div>
      </form>

      <table class="table">
            <thead class="thead-inverse">
                  <tr>
                        <th> Name</th>
                        <th> Author </th>
                        <th> Published Date </th>
                        <th> Category </th>
                        <th> User </th>
                        {{-- <th> Actions </th> --}}
                  </tr>
            </thead>
            <tbody>
                  @foreach($books as $book)
                  <tr>
                        <td scope="row"> {{$book->name}} </td>
                        <td> {{$book->author}} </td>
                        <td> {{$book->published_date}} </td>
                        <td> {{$book->category->name}} </td>
                        <td>{{ isset($book->user->name) ? $book->user->name : 'Available' }}</td>

                        <td>
                              <a type="button" class="btn btn-primary" href="{{ route('books.edit', $book->id) }}">Edit</a>
                              <a type="button" class="btn btn-danger" href="{{ route('books.destroy', $book->id) }}">Delete</a>
                              {{-- @if(!isset($book->user->name))
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-id="{{$book->id}}" data-title="{{$book->name}}" data-target="#model-{{ $book->id }}">Change Status</button>
                              @endif --}}
                        </td>
                  </tr>

                  @include('includes.modal')

            @endforeach
      </tbody>
</table>
{{ $books->links() }}
@stop
