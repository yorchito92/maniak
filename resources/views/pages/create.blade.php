@extends('layouts.app')
@section('content')
<h4>New Book</h3>
<form method="POST" action="{{ route('books.store') }}" accept-charset="UTF-8">
      @include('forms.book', ['buttonAction' => 'Save'])
</form>
@stop
