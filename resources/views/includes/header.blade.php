<div class="header clearfix">
    <h3 class="text-muted pull-left">Library App</h3>
    <a type="button" class="btn btn-primary pull-right" href="{{ route('books.index') }}">Books</a>
    <a type="button" class="btn btn-primary pull-right" href="{{ route('books.create') }}">Add Book</a>
</div>
