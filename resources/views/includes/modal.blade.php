<!-- Modal -->
<div class="modal" id="model-{{ $book->id }}" tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Set <strong>{{$book->name}}</strong> status to not available?</h4>
      </div>
      {{-- <div class="modal-body">
      {{$book->name}}
    </div> --}}
    <div class="modal-footer">
      <button type="button" class="btn btn-default" >Set to Available</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
