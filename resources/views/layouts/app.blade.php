<!DOCTYPE html>
<html lang="es">
<head>
	@include('includes.head')
</head>
<body>
	<div id="main" class="row">
		<div class="container">
			@include('includes.header')
			@include('includes.errors')
	            <div class="panel panel-default">
	                  <div class="panel-body">
        			@yield('content')
				</div>
			</div>
    	</div>
	</div>

    {{-- @include('includes.footer') --}}

	@include('includes.scripts')

</body>
</html>
