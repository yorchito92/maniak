<?php

use Illuminate\Database\Seeder;
use App\Models\Book;
use App\Models\User;
use App\Models\Category;
use Carbon\Carbon;

class BooksSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $random_user_1 = User::inRandomOrder()->first();
        $random_user_2 = User::inRandomOrder()->first();
        $random_user_3 = User::inRandomOrder()->first();
        $random_user_4 = User::inRandomOrder()->first();
        $random_user_5 = User::inRandomOrder()->first();

        $terror = Category::where('name' , '=', 'Terror')->first();
        $fantasy = Category::where('name' , '=', 'Fantasy')->first();

        Book::create([
            'name' => 'The Shining',
            'author' => 'Stephen King',
            'published_date' => Carbon::parse('1977-01-01'),
            'user_id' => $random_user_1->id,
            'category_id' => $terror->id,
        ]);
        Book::create([
            'name' => 'IT',
            'author' => 'Stephen King',
            'published_date' => Carbon::parse('1986-09-01'),
            'user_id' => $random_user_5->id,
            'category_id' => $terror->id,
        ]);
        Book::create([
            'name' => 'Carrie',
            'author' => 'Stephen King',
            'published_date' => Carbon::parse('1974-04-01'),
            'user_id' => $random_user_2->id,
            'category_id' => $terror->id,
        ]);
        Book::create([
            'name' => 'Under the Dome',
            'author' => 'Stephen King',
            'published_date' => Carbon::parse('2009-01-01'),
            'user_id' => null,
            'category_id' => $terror->id,
        ]);

        Book::create([
            'name' => 'Cabal',
            'author' => 'Clive Barker',
            'published_date' => Carbon::parse('1988-12-01'),
            'user_id' => null,
            'category_id' => $terror->id,
        ]);
        Book::create([
            'name' => 'The Hellbound Heart',
            'author' => 'Clive Barker',
            'published_date' => Carbon::parse('1991-11-01'),
            'user_id' => $random_user_3->id,
            'category_id' => $terror->id,
        ]);
        Book::create([
            'name' => 'The Shining',
            'author' => 'Clive Barker',
            'published_date' => Carbon::parse('1992-03-01'),
            'user_id' => null,
            'category_id' => $terror->id,
        ]);

        Book::create([
            'name' => 'The Hobbit',
            'author' => 'John Ronald Reuel Tolkien',
            'published_date' => Carbon::parse('1937-09-21'),
            'user_id' => $random_user_4->id,
            'category_id' => $fantasy->id,
        ]);
        Book::create([
            'name' => 'The Fellowship of the Ring',
            'author' => 'John Ronald Reuel Tolkien',
            'published_date' => Carbon::parse('1954-07-29'),
            'user_id' => $random_user_4->id,
            'category_id' => $fantasy->id,
        ]);
        Book::create([
            'name' => 'The Two Towers',
            'author' => 'John Ronald Reuel Tolkien',
            'published_date' => Carbon::parse('1954-11-11'),
            'user_id' => $random_user_4->id,
            'category_id' => $fantasy->id,
        ]);
        Book::create([
            'name' => 'The Return of the King',
            'author' => 'John Ronald Reuel Tolkien',
            'published_date' => Carbon::parse('1954-10-20'),
            'user_id' => $random_user_4->id,
            'category_id' => $fantasy->id,
        ]);
    }
}
