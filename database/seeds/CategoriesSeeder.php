<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Terror',
            'description' => 'Books with terror, horror and scary topics and stories',
        ]);
        Category::create([
            'name' => 'Fantasy',
            'description' => 'Books telling fantastic histories',
        ]);
    }
}
