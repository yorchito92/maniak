let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 var paths = {
     'bootstrap_sass': './bower_components/bootstrap-sass/assets/',
     'bootstrap': './bower_components/bootstrap/dist/'
 };

// mix.copy(paths.bootstrap_sass + 'fonts/bootstrap/**', 'public/fonts');
// mix.copy(paths.bootstrap_sass + 'stylesheets/bootstrap/**', 'resources/assets/sass/bootstrap-sass');
// mix.copy(paths.bootstrap_sass + 'stylesheets/bootstrap/mixins/**', 'resources/assets/sass/bootstrap-sass/mixins');

// mix.copy(paths.bootstrap + 'css/bootstrap.css', 'public/css');
// mix.copy(paths.bootstrap + 'css/bootstrap-theme.css', 'public/css');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
   // .sass('resources/assets/sass/app.scss', 'public/css', {includePaths: [paths.bootstrap + 'stylesheets/']});
