<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::resource('/api/v1/user', "UserController");

/**
 * Route model bindings
 */
Route::model('user', App\Models\User::class);
Route::model('book', App\Models\Book::class);
Route::model('category', App\Models\Category::class);

/**
 * User resource routes
 */
Route::get('/v1/users', 'UserController@index');
Route::get('/v1/users/{user}', 'UserController@show');

/**
 * Book resource routes
 */
Route::get('/v1/books', 'BookController@index');
Route::get('/v1/books/{book}', 'BookController@show');

/**
 * Category resource routes
 */
Route::get('/v1/categories', 'CategoryController@index');
Route::get('/v1/categories/{category}', 'CategoryController@show');
