<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:200',
            'author' => 'required|max:200',
            'published_date' => 'required',
            'category' => 'required',
        ];
    }

    /**
    * Custom error messages
    *
    */
    public function messages()
    {
        return [
            'required' => 'The :attribute of the book is required.',
        ];
    }
}
