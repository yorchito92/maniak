<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Models\Category;
use Validator;
use Carbon\Carbon;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.books')
            ->with('books', Book::paginate(5));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create')
                ->with('categories', Category::get(['id', 'name']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $splitedDate = explode("-", $request->published_date);
        $book = new Book([
            'name' => $request->name,
            'author' => $request->author,
            'published_date' => Carbon::CreateFromDate($splitedDate[0], $splitedDate[1], '1'),
        ]);
        $category = Category::find([$request->category])->first();
        $book->category()->associate($category);
        $book->save();

        return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return response()->json($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('pages.edit')
                ->with('categories', Category::get(['id', 'name']))->with('book', $book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $splitedDate = explode("-", $request->published_date);

        $book->name = $request->name;
        $book->author = $request->author;
        $book->published_date = Carbon::CreateFromDate($splitedDate[0], $splitedDate[1], '1');

        $category = Category::find([$request->category])->first();
        $book->category()->associate($category);
        $book->update();

        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        
        return redirect('/books');
    }

    /**
    * Search books by:
    *
    * Name, Author
    */
    public function search(Request $request)
    {
        $books = Book::where($request->type, 'LIKE', '%'.$request->search.'%')->paginate(5);
        return view('pages.books')
            ->with('books', $books);
    }
}
