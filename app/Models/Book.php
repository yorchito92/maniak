<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Book extends Model
{
    /**
    * Dates Array
    *
    */
    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    //     'published_date'
    // ];

    /**
    * Accessors
    *
    *
    */
    public function getPublishedDateAttribute(){
        return Carbon::parse($this->attributes['published_date'])->format('Y-m');
    }

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'author',
        'published_date',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
